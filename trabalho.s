.data
	buffer: .space 32

	#------------ messagens -----------------------------#
	Msgdisiplina:.asciiz "Digite o nome da disciplina  :"
	MsgQuantidadeAlunos:.asciiz "\nDigite a quantidade de alunos :"	
	MsgNumeroNotas:.asciiz "\nNumero de notas por alunos :"
	MsgNomeAluno:.asciiz "\nInforme o nome do aluno :"
	MsgNotaAluno:.asciiz "\nNota do Aluno :"
	QuebraLinha:.asciiz "\n -------------------------------------------------------------- \n"
    barraN :.asciiz "\n"
	
.text
.globl main
main:
    move $s0,$gp
    addi $s0,$s0,5000
	

	#registradres normais
	add $s1,$zero,$zero
	add $s2,$zero,$zero
	add $s3,$zero,$zero
    add $s4,$zero,$zero
	
	#registradres temporarios
	add $t0,$zero,$zero
	add $t1,$zero,$zero
	add $t2,$zero,$zero
    add $t3,$zero,$zero
    add $t4,$zero,$zero
	add $t5,$zero,$zero
	
	move $s2,$gp		# Move posição inicial de memória para $s2 ($s2 = end. do end. da string)
	addi $s6,$gp,600
	
	add  $t4,$t4,$s0
	add  $t0, $t0,$s2
    addi $t3,$t3,4
	addi $s3,$s3,4
		
	
	
Leitura:
    #mostra messagem disciplina
    li $v0, 4
	la $a0, Msgdisiplina
	syscall 
	
    #leitura  disciplina
	la $a0, buffer     
	li $a1, 32   
	li $v0, 8  
	syscall

    #mostra quantidade alunos messagem  
    li $v0, 4
	la $a0, MsgQuantidadeAlunos
	syscall 
	
    #leitura quantidade alunos
	li $v0, 5 
    syscall
	#guardando valor registrador
    add $t1,$t1,$v0
    
	#mostra quantidade notas dos alunos messagem  	
	li $v0, 4
	la $a0, MsgNumeroNotas
	syscall 
	
    #leitura   quantidade notas dos  alunos  inteiro
	li $v0, 5 
    syscall
	
    #guardando valor registrador
	add $t2,$t2,$v0
	
    mul $t3,$t3,$t2
    mul $s3,$s3,$t1
    add $t0,$t0,$s3 
LoopAlunos:



	div $s4,$s4,$t2 #divide o valor da media

	sw $s4,0($s0) # save no array de notas
    add $s4,$zero,$zero
	
	beq $s2 ,$t0 ,Exit

    #-------   
    add $t4,$t4,$t3   
	
	li $v0, 4
	la $a0, MsgNomeAluno  
    syscall
	
	#leitura  nome aluno
	li $v0, 8          # indica a funcao 
	la $a0,0($s6)     # o endereco 
	li $a1, 32         # tamanho    
	syscall
	
    move $s1,$a0
    sw $s1,0($s2) 
	
	addi $s2,$s2,4 
	addi $s6,$s6,32

	
LoopNotas :    
 
    beq $s0,$t4,LoopAlunos 
    li $v0, 4
    la $a0, MsgNotaAluno  
    syscall

    li $v0, 5 
    syscall       
	
    addi $s0,$s0,4 
    add $s4,$s4,$v0

    j LoopNotas

Exit:
  
Resultado:	 
    
	li $v0, 4
    la $a0, QuebraLinha  
    syscall


    li $v0, 4  
    la $a0, buffer    
    syscall
	
	
    sub $t0,$t0,$s3
	
	add $s6,$zero,$zero
	addi $s6,$s6,2
	
	mul $t3,$t3,$s6
	
    sub $s0,$s0,$t3
	
ResultadoFinal:

    beq $s2 ,$t0 ,Exit2
     
    lw $t1,0($t0)	# Lê do array os nomes

	li $v0,4		    # Define escrita de um tipo String
	move $a0,$t1		# Escreve os nomes na tela
	syscall
	   
	   

	lw $t1,0($s0)
	li $v0,1		    # Define escrita de um tipo String
	move $a0,$t1		# Escreve os nomes na tela
	syscall
	   	addi $s0,$s0,8
	#contador da nomes
    addi $t0,$t0,4
       
    li $v0, 4          # codigo para quebrar linha somente
    la $a0, barraN  
    syscall		
		
    j ResultadoFinal
	   
Exit2: